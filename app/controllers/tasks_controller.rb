class TasksController < ApplicationController

  def index
    tasks = Task.includes(:tags).all
    render json: tasks
  end

  def create
    task = Task.create(task_params)

    if task.save
      render json: task
    else
      render_errors task
    end
  end

  def update
    task = Task.find(params[:data][:id])

    if task.update(task_params)
      render json: task
    else
      render_errors task
    end
  end

  private

  def task_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(
      params,
      only: [:title, :tags],
      keys: { tags: :tags_titles }
    )
  end

end
