class ApplicationController < ActionController::API

  protected

  def render_errors(object)
    render json: object, serializer: ActiveModel::Serializer::ErrorSerializer
  end

end
