class TagsController < ApplicationController

  def index
    tags = Tag.all
    render json: tags
  end

  def create
    tag = Tag.create(tag_params)

    if tag.save
      render json: tag
    else
      render_errors tag
    end
  end

  def update
    tag = Tag.find(params[:data][:id])

    if tag.update(tag_params)
      render json: tag
    else
      render_errors tag
    end
  end

  private

  def tag_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
  end

end
