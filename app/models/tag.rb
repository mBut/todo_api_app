class Tag < ApplicationRecord
  belongs_to :task, optional: true

  validates :title, presence: true, uniqueness: true
end
