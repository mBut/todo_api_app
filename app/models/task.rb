class Task < ApplicationRecord
  validates :title, presence: true

  has_many :tags

  def tags_titles=(titles)
    self.tags = titles.map{ |title| Tag.where(title: title).first_or_initialize }
  end

end
