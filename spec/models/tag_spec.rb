require 'rails_helper'

RSpec.describe Tag, type: :model do

  describe  "validations" do
    it "is invalid without title" do
      tag = Tag.new

      expect(tag.valid?).to be_falsy
      expect(tag.errors.details[:title]).to eq [{error: :blank}]
    end

    it "is invalid if such title is not unique" do
      Tag.create!({title: "Tag 1"})

      tag = Tag.new({title: "Tag 1"})
      expect(tag.valid?).to be_falsy
      expect(tag.errors.details[:title]).to eq [{:error=>:taken, :value=>"Tag 1"}]
    end
  end

end
