require 'rails_helper'

RSpec.describe Task, type: :model do

  describe  "validations" do
    it "is invalid without title" do
      task = Task.new

      expect(task.valid?).to be_falsy
      expect(task.errors.details[:title]).to eq [{error: :blank}]
    end
  end

  describe "#tags_titles=" do
    it "should initialize new tag if no tags with given title were created" do
      task = Task.new

      task.tags_titles = ["Some tag"]
      expect(task.tags.size).to be 1
      expect(task.tags[0].id).to be_nil
      expect(task.tags[0].title).to eq "Some tag"
    end

    it "should find tag if tag with similar title already exists" do
      Tag.create({title: "Some tag"})

      task = Task.new

      task.tags_titles = ["Some tag"]
      expect(task.tags.size).to be 1
      expect(task.tags[0].id).not_to be_nil
      expect(task.tags[0].title).to eq "Some tag"
    end
  end
end
