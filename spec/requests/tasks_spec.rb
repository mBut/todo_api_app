require "rails_helper"

RSpec.describe "Tasks API", :type => :request do
  context "GET /api/v1/tasks" do
    before do
      Task.create!({title: "Some task 1"})
      Task.create!({title: "Some task 2"})
    end

    it "shows list of the tasks" do
      get "/api/v1/tasks"
      data = parse_json(response.body)["data"]

      expect(data.size).to eq 2
      expect(data[1]["id"]).to eq "2"
      expect(data[1]["attributes"]["title"]).to eq "Some task 2"
    end
  end

  context "POST /api/v1/tasks" do
    it "creates a new task when params are valid" do
      params = {
        "data": {
          "id": "undefined",
          "type": "undefined",
          "attributes": {
            "title": "Wash laundry"
          }
        }
      }

      post "/api/v1/tasks", params: params

      data = parse_json(response.body)["data"]
      expect(data["id"]).to eq "1"
      expect(data["attributes"]["title"]).to eq "Wash laundry"
    end

    it "return errors when params are invalid" do
      params = {
        "data": {
          "id": "undefined",
          "type": "undefined",
          "attributes": {
            "title": ""
          }
        }
      }

      post "/api/v1/tasks", params: params

      errors = parse_json(response.body)["errors"]
      expect(errors[0]["source"]["pointer"]).to eq "/data/attributes/title"
      expect(errors[0]["detail"]).to eq "can't be blank"
    end
  end

  context "PATCH /api/v1/tasks" do
    before do
      @task = Task.create({title: "Some task"})
    end

    it "updates a task when params are valid" do
      params = {
        "data": {
          "type":"tasks",
          "id": @task.id.to_s,
          "attributes":{
            "title":"Updated Task Title"
          }
        }
      }

      post "/api/v1/tasks", params: params

      data = parse_json(response.body)["data"]
      expect(data["attributes"]["title"]).to eq "Updated Task Title"
    end

    it "adds correct tag to the task" do
      params = {
        "data": {
          "type":"tasks",
          "id": @task.id.to_s,
          "attributes":{
            "title":"Updated Task Title",
            "tags": ["Urgent", "Home"]
          }
        }
      }

      post "/api/v1/tasks", params: params

      data = parse_json(response.body)["data"]
      expect(data["attributes"]["title"]).to eq "Updated Task Title"

      tags = data["relationships"]["tags"]["data"]
      expect(tags.size).to eq 2
    end

    it "return errors when params are invalid" do
      params = {
        "data": {
          "type":"tasks",
          "id": @task.id.to_s,
          "attributes":{
            "title":""
          }
        }
      }

      post "/api/v1/tasks", params: params

      errors = parse_json(response.body)["errors"]
      expect(errors[0]["source"]["pointer"]).to eq "/data/attributes/title"
      expect(errors[0]["detail"]).to eq "can't be blank"
    end
  end
end
