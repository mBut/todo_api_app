require "rails_helper"

RSpec.describe "Tags API", :type => :request do
  context "GET /api/v1/tags" do
    before do
      Tag.create!({title: "Foo"})
      Tag.create!({title: "Bar"})
    end

    it "shows list of the tags" do
      get "/api/v1/tags"
      data = parse_json(response.body)["data"]

      expect(data.size).to eq 2
      expect(data[1]["id"]).to eq "2"
      expect(data[1]["attributes"]["title"]).to eq "Bar"
    end
  end

  context "POST /api/v1/tags" do
    it "creates a new tag when params are valid" do
      params = {
        "data": {
          "id": "undefined",
          "type": "undefined",
          "attributes": {
            "title": "Wash laundry"
          }
        }
      }

      post "/api/v1/tags", params: params

      data = parse_json(response.body)["data"]
      expect(data["id"]).to eq "1"
      expect(data["attributes"]["title"]).to eq "Wash laundry"
    end

    it "return errors when params are invalid" do
      params = {
        "data": {
          "id": "undefined",
          "type": "undefined",
          "attributes": {
            "title": ""
          }
        }
      }

      post "/api/v1/tags", params: params

      errors = parse_json(response.body)["errors"]
      expect(errors[0]["source"]["pointer"]).to eq "/data/attributes/title"
      expect(errors[0]["detail"]).to eq "can't be blank"
    end
  end

  context "PATCH /api/v1/tags" do
    before do
      @tag = Tag.create({title: "Some tag"})
    end

    it "updates a task when params are valid" do
      params = {
        "data": {
          "type":"tags",
          "id": @tag.id.to_s,
          "attributes":{
            "title":"Updated Tag Title"
          }
        }
      }

      post "/api/v1/tags", params: params

      data = parse_json(response.body)["data"]
      expect(data["attributes"]["title"]).to eq "Updated Tag Title"
    end
  end
end
